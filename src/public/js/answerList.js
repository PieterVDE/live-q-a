$(document).ready(function () {

    /**
     * Global vars
     */
    var socket = io.connect('localhost:3000');
    var loggedInUser = JSON.parse(localStorage.getItem('user'));

    var init = function () {
        transformTimestamp();
    }

    var transformTimestamp = function () {
        //        console.info('Tranform timestamp');
        var data = $('.data-timestamp');

        for (var i = data.length - 1; i >= 0; i--) {
            //            console.log(i);
            var id = $(data[i]).data('id');
            //            console.log(id);
            var obj = $('#' + id);
            var text = obj.text();
            //            console.log(text);
            obj.text(moment(text).startOf('minute').fromNow());
        }

        // /(var d = $('.data-timestamp')).val(moment(d).startOf('minute').fromNow());
    }


    /**
     * Service die answer gerelateerde functies mapt
     */
    var answerService = (function () {

        var displayAnswer = function (data) {

            var answer = data.answer;
            var urlArray = convertUrl(answer.answer);

            console.log("URL AFTER FUNCTION ", urlArray);

            if (urlArray.linkArray.length > 0) {
                //Answer should be converted into img
                for (var i = 0, l = urlArray.linkArray.length; i < l; i++) {
                    console.log(answer.answer.search(urlArray.urlArray[i]));
                    answer.answer.replace(urlArray.urlArray[i], urlArray.linkArray[i]);
                }
                console.log("answer after replace", answer.answer);
            }

            var output =
                '<div class="answer">' +
                '<div class="answer-text">' + answer.answer +
                '<span class="author">- ' + answer.author.firstname + ' ' + answer.author.lastname + '</span>' +
                '<span class="timestamp">' + moment(answer.time).startOf('minute').fromNow() + '</span>' +
                '</div>' +
                '</div>';

            $('.answers', $('#' + data.questionID).closest('.question')).prepend(output);
        }

        var addAnswer = function (answer, questionID, discussionID) {

            var output = {
                discussionID: discussionID,
                questionID: questionID,
                answer: {
                    author: {
                        firstname: loggedInUser.name.givenName,
                        lastname: loggedInUser.name.familyName,
                        facebookId: loggedInUser.id,
                    },
                    answer: answer,
                    votes: 0
                }
            };
            console.log("addAnswer", output);

//            displayAnswer(output.answer);

            socket.emit('addAnswer', output, function (error, data) {
//                displayAnswer(data);
            });
        }

        return {
            displayAnswer: displayAnswer,
            addAnswer: addAnswer
        }

    })();

    init();

    var convertUrl = function (str) {
        var source = (str || '').toString();
        var arrays = {
            urlArray: [],
            linkArray: []
        }
        var url;
        var matchArray;

        // Regular expression to find FTP, HTTP(S) and email URLs.
        var regexToken = /(((ftp|https?):\/\/)[\-\w@:%_\+.~#?,&\/\/=]+)|((mailto:)?[_.\w-]+@([\w][\w\-]+\.)+[a-zA-Z]{2,3})/g;

        // Iterate through any URLs in the text.
        while ((matchArray = regexToken.exec(source)) !== null) {
            var token = matchArray[0];
            arrays.urlArray.push(token);
        }

        //Check for images in urls
        for (var i = 0, l = arrays.urlArray.length; i < l; i++) {
            arrays.linkArray.push(checkExtension(arrays.urlArray[i]));
        }

        function checkExtension(url) {
            var extension = url.substr((url.lastIndexOf('.') + 1));
            switch (extension) {
            case 'jpg':
            case 'png':
            case 'gif':
                url = '<a class="answer-link" href="' + url + '"><img class="answer-img" src="' + url + '" /></a>';
                break;
            default:
                url = '<a class="answer-link" href="' + url + '">' + url + '</a>';
                break;
            }
            return url;
        };

        //Convert all other urls to links
        return arrays;
    }

    /**
     * Event handlers
     */
    var onAddAnswer = function (inputfield) {

        var answer = inputfield.val();

        var questionID = inputfield.attr('id');
        var discussionID = $('#topicID').val();

        if (answer && loggedInUser) {
            answerService.addAnswer(answer, questionID, discussionID);
        } else {
            console.error('User not logged in');
        }

        // socket.emit('addAnswer', answer);
    }

    $(".add-answer-input").keyup(function (e) {
        //        console.log('key-up');
        if (e.keyCode == 13) {
            onAddAnswer($(this));
        }
    });

    /**
     * Socket handlers
     */
    socket.on('returnAnswers', function (answers) {
        for (var i = 0, l = answers.length; i < l; i++) {
            var a = answers[i].obj;
            answerService.displayAnswer(a);
        }
    });

    socket.on('newAnswer', function (answer) {

        console.log('On newAnswer', answer);

        answerService.displayAnswer(answer);
    });
});
