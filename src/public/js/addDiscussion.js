$(document).ready(function () {

	var socket = io.connect('localhost:3000');

    var loggedInUser = JSON.parse(localStorage.getItem('user'));
    console.log(loggedInUser);

	var dicussion = {
		"location": "",
		"author": {
            firstname: loggedInUser.name.givenName,
            lastname: loggedInUser.name.familyName,
            facebookId: loggedInUser.id
		},
		"topic": ""
	};

	if (navigator.geolocation) {
        navigator.geolocation.getCurrentPosition(function(location) {
        	console.log('Emit: ' + location)
        	dicussion.location = location;
        });
    }

    $('#submit').click(function() {
    	dicussion.topic = $('#topic').val();
    	console.log(dicussion);
    	socket.emit('addDiscussion', dicussion);
    });

});