$(document).ready(function () {

    /**
     * Global vars
     */
    var socket = io.connect('localhost:3000');
    var loggedInUser = JSON.parse(localStorage.getItem('user'));

    var init = function () {
        transformTimestamp();

        if (!loggedInUser) {
            console.log('User not logged in');
            $('#myModal').modal('show');
        } else {
            console.log(localStorage.getItem('user'));
        }
    }

    var transformTimestamp = function () {
        //        console.info('Tranform timestamp');
        var data = $('.data-timestamp');

        for (var i = data.length - 1; i >= 0; i--) {
            //            console.log(i);
            var id = $(data[i]).data('id');
            //            console.log(id);
            var obj = $('#' + id);
            var text = obj.text();
            //            console.log(text);
            obj.text(moment(text).startOf('minute').fromNow());
        }

        // /(var d = $('.data-timestamp')).val(moment(d).startOf('minute').fromNow());
    }

    /**
     * Service die question gerelateerde functies mapt
     */
    var questionService = (function () {

        var displayQuestion = function (question) {

            var output =
                '<div class="col-md-3">' +
                    '<div class="question">' +
                        '<div class="content">' +
                            '<p>' + question.question + '</p>' +
                            '<div class="icons">' +
//                                '<i class="fa fa-chevron-up fa-2x"></i>' +
//                                '<i class="fa fa-chevron-down fa-2x"></i>' +
                            '</div>' +
                        '</div>' +
                        '<footer>' +
                            '<div class="text">' +
                                '<p class="timestamp"> Asked ' + moment(question.time).startOf('minute').fromNow() + '</span></p>' +
                                '<p class="author"> by ' + question.author.firstname + ' ' + question.author.lastname + '</p>' +
                            '</div>' +
                            '<img class="img-author" src="http://graph.facebook.com/' + question.author.facebookId + '/picture?type=large"/>' +
                        '</footer>' +
                        '<div class="answers">' +
                            '<div class="add-answer">' +
                                '<input type="text" id="' + question._id + '" placeholder="Answer this question" class="add-answer-input">' +
                            '</div>' +
                        '</div>' +
                    '</div>' +
                '</div>';

            $('#questions').prepend(output);
            location.reload();
        }

        var addQuestion = function (topic, topicID) {

            var output = {
                id: topicID,
                question: {
                    author: {
                        firstname: loggedInUser.name.givenName,
                        lastname: loggedInUser.name.familyName,
                        facebookId: loggedInUser.id,
                    },
                    question: topic,
                    votes: 0,
                    answers: []
                }
            };
            //            console.log(output);

            socket.emit('addQuestion', output, function (error, data) {
                //displayQuestion(data);
            });
        }

        return {
            displayQuestion: displayQuestion,
            addQuestion: addQuestion
        }

    })();

    init();

    /**
     * Event handlers
     */
    var onAddQuestion = function (e) {

        var topic = $('#topic').val();
        var topicID = $('#topicID').val();

        if (topic && loggedInUser) {
            questionService.addQuestion(topic, topicID);
        } else {
            console.error('User not logged in');
        }

        // socket.emit('addDiscussion', dicussion);
    }

    $('#submit').click(onAddQuestion);


    /**
     * Socket handlers
     */
    socket.on('returnQuestions', function (questions) {
        for (var i = 0, l = questions.length; i < l; i++) {
            var d = questions[i].obj;
            questionService.displayQuestion(d);
        }
    });

    socket.on('newQuestion', function (question) {

        console.log('On newQuestion', question);

        questionService.displayQuestion(question);
    });

    /**
     * Lock a question - hide answer input, close votes
     */

    var onLockQuestion = function (e) {
        //        console.log("onLockQuestion", e);
        var i = $(e.currentTarget);
        i.toggleClass("fa-unlock-alt fa-lock");
        i.closest(".question").find('.add-answer-input').toggle();
    }

    $('.lock-question').click(onLockQuestion);
});
