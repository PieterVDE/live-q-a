$(document).ready(function () {

    /**
     * Global vars
     */
    var socket = io.connect('localhost:3000');
    var loggedInUser = JSON.parse(localStorage.getItem('user'));

    /** 
     * Init functie
     */
    var init = function() {
        /**
         * Location vragen aan browser
         */
        locationService.getLocation(function(location) {

            if(location) {

                // Stuur locatie naar server en vraag topics
                console.log('Emit: ', location)
                socket.emit('getNearDiscussions', location);

                // Call naar google voor locatie
                locationService.getHumanReadableLocation(location, function(res) {

                    $('#title').text("Topics around " + res.results[0].formatted_address);

                });
            }

        });

        if(!loggedInUser) {
            console.log('User not logged in');
            $('#myModal').modal('show');
        }
        else {
            console.log(localStorage.getItem('user'));
        }
    }

    /** 
     * Sercice die locatie gerelateerde functies mapt
     */
    var locationService = (function() {

        /** 
         * Zoek een human readable address van latitude en longitude
         */
        var getHumanReadableLocation = function (location, callback) {

            var service = 'https://maps.googleapis.com/maps/api/geocode/json?latlng=' + location.coords.latitude + ',' + location.coords.longitude + '&key=AIzaSyCxu51hlWG6dB0S8ZxbYpBsmc8_C_Bnc4o';
                    
            $.ajax({
                type: 'GET',
                dataType: 'json',
                url: service
            }).done(function (res) {

                callback(res);

            }).fail(function (error) {

                console.log(error);

            }).always(function () {

                console.log('Stop loading');

            });
        }

        /** 
         * Get location from browser
         */
        var getLocation = function(callback) {
            if (navigator.geolocation) {

                navigator.geolocation.getCurrentPosition(function(location) {

                    callback(location);

                });
            }
            else {
                return false
            }
        }

        return {
            getLocation: getLocation,
            getHumanReadableLocation: getHumanReadableLocation
        }
    })();
   
    /** 
     * Sercice die discussie gerelateerde functies mapt
     */
    var discussionService = (function() {

        var displayDiscussion = function(discussion) {

            var output  =   '<div class="col-md-4">';
                output +=       '<a class="no-style" href="/discussion/' + discussion.obj._id + '/questions">';
                output +=           '<div class="discussion">';
                output +=               '<div class="content">' + discussion.obj.topic + '</div>';
                output +=               '<footer>';
                output +=                   '<span>' + Math.round(discussion.dis) + ' meter </span>';
                output +=                   '<span class="label label-success">' + moment(discussion.obj.time).startOf('minute').fromNow() + '</span>';
                output +=               '</footer>';
                output +=           '</div>';
                output +=       '</a>';
                output +=   '</div>';

                //moment(discussion.time).startOf('minute').fromNow()

            $('#discussions').prepend(output);
        }

        var addDiscussion = function(location, author, topic) {

            var dicussion = {
                "location": location,
                "author": {
                    firstname: author.name.givenName,
                    lastname: author.name.familyName,
                    facebookId: author.id
                },
                "topic": topic
            };

            socket.emit('addDiscussion', dicussion, function(error, data) {
                //displayDiscussion(data);
            });
        }

        return {
            displayDiscussion: displayDiscussion,
            addDiscussion: addDiscussion
        }

    })();

    /**
     * Call init --> start app
     */
     init();

    /** 
     * Event handlers
     */
    var onAddTopic = function(e) {

        var topic = $('#topic').val();
        
        if(topic && loggedInUser) {
            // Locatie vragen --> we kunnen er niet vanuitgaan dat dit al is gebeurd. >> Kijken hoe dit misschien op voorhand gecached kan worden
            locationService.getLocation(function(location) { 

                discussionService.addDiscussion(location, loggedInUser,topic);

            });
        }
        


        
        // socket.emit('addDiscussion', dicussion);
    }

    $('#submit').click(onAddTopic);
    

    /** 
     * Socket handlers
     */
    socket.on('returnNearDiscussions', function(discussions, stats) {

        console.log(discussions);

        // hide loader
        $('#loader').hide();

        for(var i = 0, l = discussions.length; i < l; i++) {
            var d = discussions[i];
            discussionService.displayDiscussion(d);
        }
    });

    /** 
     * Socket handlers
     */
    socket.on('newDiscussion', function(discussion) {

        console.log('On newDiscussion', discussion);

        var d = {
            "obj": discussion,
            "dis": 0
        };

        discussionService.displayDiscussion(d);

    });
});
