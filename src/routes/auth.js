var express = require('express');
var router = express.Router();
var passport = require('passport');

/* GET home page. */
router.get('/facebook', passport.authenticate('facebook', { scope: ['public_profile']}),
    function(req, res){
});

router.get('/facebook/callback', passport.authenticate('facebook', { 
	    failureRedirect: '/fail' 
	}),
	function(req, res) {
	    console.log('Is het? ' + req.isAuthenticated())
	    res.redirect('/discussion');
	});

module.exports = router;
