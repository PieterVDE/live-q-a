var express = require('express');
var router = express.Router();
var functions = require('../functions.js');
var Discussion = require('../models/Discussion');

/* GET home page. */
router.get('/', function(req, res, next) {
    res.render('discussion/discussionList', {'user': req.user});
});

router.get('/add', function(req, res, next) {
    res.render('discussion/addDiscussion', {'user': req.user});
});

router.get('/:id/questions', function (req, res, next) {
	var id = req.params.id;

    Discussion.findById(id, function(error, topic){
        res.render('discussion/questionList', {'user': req.user, 'topicID': id, 'questions': topic.questions, 'title': topic.topic});
    });

});

module.exports = router;
