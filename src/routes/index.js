var express = require('express');
var router = express.Router();
var functions = require('../functions.js');

/* GET home page. */
router.get('/', function(req, res, next) {

    console.log('Homepage routing started');

    var getLocalIp = function() {
        'use strict';

        var os = require('os');
        var ifaces = os.networkInterfaces();
        var output = 'test';

        Object.keys(ifaces).forEach(function (ifname) {
            var alias = 0;

            ifaces[ifname].forEach(function (iface) {
                if ('IPv4' !== iface.family || iface.internal !== false) {
                    // skip over internal (i.e. 127.0.0.1) and non-ipv4 addresses
                    return iface.address;
                }

                if (alias >= 1) {
                    // this single interface has multiple ipv4 addresses
                    console.log(ifname + ':' + alias, iface.address);
                } else {
                    // this interface has only one ipv4 adress
                    console.log('Hierzo');
                    output = iface.address;
                    console.log(ifname, iface.address);
                    return 'helololo';
                }
            });
        });

        return output;
    };

    console.log(getLocalIp());

    res.render('index', { title: 'Live Q & A', ip: getLocalIp(), user: req.user});
});

router.get('/account', functions.ensureAuthenticated, function(req, res) {

    console.log(req.user);

    res.render('account', { user: req.user });
});

router.get('/logout', function(req, res){
    req.logout();
    res.redirect('/');
});

module.exports = router;
