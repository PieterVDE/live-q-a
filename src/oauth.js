var ids = {
    facebook: {
        clientID: '879426678789500',
        clientSecret: '7e428fb54853411c41da7811b71b254f',
        callbackURL: 'http://localhost:3000/auth/facebook/callback'
    },
    twitter: {
        consumerKey: 'get_your_own',
        consumerSecret: 'get_your_own',
        callbackURL: "http://127.0.0.1:1337/auth/twitter/callback"
    },
    github: {
        clientID: 'get_your_own',
        clientSecret: 'get_your_own',
        callbackURL: "http://127.0.0.1:1337/auth/github/callback"
    },
    google: {
        returnURL: 'http://127.0.0.1:1337/auth/google/callback',
        realm: 'http://127.0.0.1:1337'
    }
}

module.exports = ids