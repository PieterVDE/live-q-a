var mongoose = require('mongoose');

var discussionSchema = mongoose.Schema({
    topic: String,
    place: {
        'type': {
            type: String,
            required: true,
            enum: ['Point', 'LineString', 'Polygon'],
            default: 'Point'
        },
        coordinates: [Number],
    },
    author: {
        firstname: String,
        lastname: String,
        facebookId: String,
        pictureUrl: String
    },
    time: {
        type: Date,
        default: Date.now
    },
    questions: [{
        id: String,
        author: {
            firstname: String,
            lastname: String,
            facebookId: String,
        },
        question: String,
        time: {
            type: Date,
            default: Date.now
        },
        votes: Number,
        answers: [{
            author: {
                firstname: String,
                lastname: String,
                facebookId: String,
            },
            answer: String,
            time: {
                type: Date,
                default: Date.now
            },
            votes: Number
        }]
    }]
})
discussionSchema.index({
    place: '2dsphere'
});
var Discussion = mongoose.model('Discussion', discussionSchema);

module.exports = Discussion;
