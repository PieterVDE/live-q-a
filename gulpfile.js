/**
 * Basic GULP setup
 */

/**
 * Define source and build path
 */
var sourceDir = 'src/';
var buildDir = 'build/';

var gulp = require('gulp'),
    imagemin = require('gulp-imagemin'),
    jshint = require('gulp-jshint'),
    sass = require('gulp-ruby-sass'),
    uglify  = require('gulp-uglify'),
    connect = require('gulp-connect')
    minifyHTML = require('gulp-minify-html'),
    concat = require('gulp-concat'),
    nodemon = require('gulp-nodemon');


/**
 * Development sass compiling and livereload
 */
gulp.task('sass', function() { //main scss file compilen
    return sass(sourceDir + '/public/scss', {
        'style': 'expanded', 
        'lineNumbers': true
    })
    .on('error', function (err) {
        console.error('Error!', err.message);
    })
    .pipe(gulp.dest(sourceDir + '/public/css'))

    .pipe(connect.reload());
});

/**
 * Production sass compiling / build
 */
gulp.task('sass_build', function() {
    return sass(sourceDir + 'scss/**/*.scss', {
        'style': 'compressed'
    })
    .on('error', function (err) {
        console.error('Error!', err.message);
    })
    .pipe(gulp.dest(buildDir + 'css'));
});

/**
 * Move Font Awesome icons to public/fonts
 */
gulp.task('icons', function() {
    return gulp.src(config.bowerDir + '/fontawesome/fonts/**.*')
        .pipe(gulp.dest('./public/fonts'));
});

/**
 * Production image optimisation
 */
gulp.task('imagemin', function(){
    gulp.src(sourceDir + 'img/*') //minify images
        .pipe(imagemin())
        .on('error', function (err) {
            console.error('Error!', err.message);
        })
        .pipe(gulp.dest(buildDir + 'img/')); //minified images will be stored here
});

/**
 * Production js minify
 */
gulp.task('js_uglify', function(){ 
    gulp.src(sourceDir + 'js/*.js') //alle filesn in js/ uglifien
    
    .pipe(uglify())
    .pipe(gulp.dest(buildDir + 'js/'))
});

/**
 * Production JS concat
 */
gulp.task('js_concat', function() {
  return gulp.src(sourceDir + 'js/*.js')
    .pipe(concat('scripts.js'))
    .pipe(gulp.dest(buildDir + 'js/'));
});

/**
 * Production JS concat + js minify
 * http://stackoverflow.com/questions/24591854/using-gulp-to-concatenate-and-uglify-files
 */
gulp.task('js_build', function() {
    return gulp.src(sourceDir + 'js/*.js')
    .pipe(concat('scripts.js', {newLine: '\r\n'}))
    .pipe(gulp.dest(buildDir + 'js/'))
    .pipe(uglify())
    .pipe(gulp.dest(buildDir + 'js/'));
});

/**
 * Development JS linitng and livereload
 */
gulp.task('jshint', function() {
    console.log('JSHINT');
    gulp.src(sourceDir + 'js/*.js') 
    .pipe(jshint())
    .pipe(jshint.reporter('default')) // print errors
    .pipe(connect.reload());
});

/**
 * Development HTML livereload
 */
gulp.task('html', function(){
    gulp.src(sourceDir + '*.html')
    .on('error', function (err) {
        console.error('Error!', err.message);
    })
    .pipe(connect.reload());
});

/**
 * Production HTML minify
 */
gulp.task('minify_html', function() {
    gulp.src(sourceDir + '*.html')
    .pipe(minifyHTML({
        empty:true,
        spare:true
    }))
    .pipe(gulp.dest(buildDir))
});

// development server en livereload
gulp.task('connect', function() {
    connect.server({
        root: 'src',
        host: 'localhost',
        livereload: true
    });
});


//watch alle scss, js en html files en als er een verandering is, ga je de taak tussen de haken uitvoeren
gulp.task('watch', function(){ 
    console.log('watch started');
    gulp.watch(sourceDir + '/public/scss/**/*.scss', ['sass']); // scss bestand aangepast? --> sass uitvoeren
    gulp.watch(sourceDir + 'js/*.js', ['jshint']); // js bestand aangepast? --> js en jshint uitvoeren
    gulp.watch(sourceDir + '*.html', ['html']); // html -> ...
});

/**
 *  Nodemon
 */
gulp.task('demon', function () {
  nodemon({
    script: sourceDir + '/bin/www',
    ext: 'js',
    env: {
      'NODE_ENV': 'development'
    }
  })
    .on('start', ['watch'])
    .on('change', ['watch'])
    .on('restart', function () {
      console.log('restarted!');
    });
});

gulp.task('default',['demon', 'watch']);
gulp.task('gulp-no-nodemon',['connect', 'watch']);

// build script
gulp.task('build',['sass_build', 'minify_html', 'imagemin', 'js_build']);